import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import { fetchCampaignStats } from './requests/campaigns';
import parseCsv from './helpers/parseCampaignStatsCsv';

import { CampaignData, CampaignFilters } from './types';
import Content from './components/Content';
import Sidebar from './components/Sidebar';

const Wrapper = styled.div`
    min-height: 100vh;
    display: flex;
`;

const initialCampaignData = {
    isLoading: true,
    campaignList: [],
    datasourceList: [],
    stats: []
}

const initialCampaignFilters = {
    campaigns: [],
    datasources: []
}

function App() {
    const [campaignData, setCampaignData] = useState<CampaignData>(initialCampaignData);
    const [campaignFilters, setCampaignFilters] = useState<CampaignFilters>(initialCampaignFilters);

    const {
        isLoading,
        campaignList,
        datasourceList,
        stats
    } = campaignData;

    useEffect(() => {
        fetchCampaignStats().then(textPromise => {
            const csvData = parseCsv(textPromise);
            setCampaignData({...csvData, isLoading: false});
        }).catch(err => {
            console.error(err);
            setCampaignData(campaignData => ({...campaignData, isLoading: false}));
        })
    }, []);

    return (
        <Wrapper>
            <Sidebar
                campaignsList={campaignList}
                datasourceList={datasourceList}
                isLoading={isLoading}
                campaignFilters={campaignFilters}
                onApply={(newCampaignFilters) => {setCampaignFilters(newCampaignFilters)}}
            />
            <Content
                campaignFilters={campaignFilters}
                stats={stats}
            />
        </Wrapper>
    );
}

export default App;
