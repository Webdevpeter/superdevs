import { CampaignStats } from '../types';

const parseCampaignStatsCsv = (csvText: string) => {
    const csvRows = csvText.split('\n');

    csvRows.splice(0, 1);

    const datasourceList: string[] = [];
    const campaignList: string[] = [];
    const stats = csvRows.reduce((campaignStats: CampaignStats[], row: string) => {
        const splitRow = row.split(',')

        // Filter out incorrect rows
        if (splitRow.length !== 5) {
            return campaignStats;
        }

        const Datasource = splitRow[1];
        if (!datasourceList.includes(Datasource)) {
            datasourceList.push(Datasource);
        }

        const Campaign = splitRow[2];
        if (!campaignList.includes(Campaign)) {
            campaignList.push(Campaign);
        }

        return [
            ...campaignStats,
            {
                Date: splitRow[0],
                Datasource,
                Campaign,
                Clicks: parseInt(splitRow[3], 10) || 0,
                Impressions: parseInt(splitRow[4], 10) || 0,
            }
        ];
    }, [])

    return { datasourceList, campaignList, stats}
}

export default parseCampaignStatsCsv;