const printFilters = (filters: string[]) => {
    return filters.length ? `"${filters.join('" and "')}"` : 'All';
}

export default printFilters;