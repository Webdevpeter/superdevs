import { CampaignFilters, CampaignStats } from "../types";

const filterCampaignStats = (campaignFilters: CampaignFilters, stats: CampaignStats[]) => {
    const { campaigns, datasources } = campaignFilters;
    
    if (campaigns.length && !datasources.length) {
        return stats.filter(singleStats => campaigns.includes(singleStats.Campaign));
    }

    if (!campaigns.length && datasources.length) {
        return stats.filter(singleStats => datasources.includes(singleStats.Datasource));
    }

    if (campaigns.length && datasources.length) {
        return stats.filter(singleStats => campaigns.includes(singleStats.Campaign) && datasources.includes(singleStats.Datasource));
    }

    return stats
}

export default filterCampaignStats;