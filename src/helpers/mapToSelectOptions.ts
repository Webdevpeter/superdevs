const mapToSelectOptions = (options: string[]) => {
    return options.map(option => ({
        value: option,
        label: option
    }))
}

export default mapToSelectOptions;