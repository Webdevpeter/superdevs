import { CampaignStats } from "../types";

interface ChartDataElement {
    Date: string;
    Clicks: number;
    Impressions: number;
}

const mapToChartData = (stats: CampaignStats[]) => {
    let dateBuffer: string;

    return stats.reduce((chartData, singleStats) => {
        if (dateBuffer === singleStats.Date) {
            chartData[chartData.length - 1].Clicks += singleStats.Clicks;
            chartData[chartData.length - 1].Impressions += singleStats.Impressions
            return chartData;
        }

        dateBuffer = singleStats.Date;
        return [...chartData, {
            Date: singleStats.Date,
            Clicks: singleStats.Clicks,
            Impressions: singleStats.Impressions
        }];
    }, [] as ChartDataElement[]);
}

export default mapToChartData;