export interface CampaignStats {
    Date: string;
    Datasource: string;
    Campaign: string;
    Clicks: number;
    Impressions: number;
}

export interface CampaignData {
    isLoading: boolean;
    campaignList: string[];
    datasourceList: string[];
    stats: CampaignStats[];
}

export interface SelectOption {
    value: string;
    label: string;
}

export interface CampaignFilters {
    campaigns: string[];
    datasources: string[];
}