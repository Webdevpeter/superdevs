const fetchCampaignStats = (): Promise<string> => {
    return fetch('http://adverity-challenge.s3-website-eu-west-1.amazonaws.com/DAMKBAoDBwoDBAkOBAYFCw.csv')
    .then(response => response.blob())
    .then(blob => blob.text())
}

export { fetchCampaignStats };