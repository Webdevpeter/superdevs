import { CampaignFilters } from "../../types";

export interface SidebarProps {
    isLoading: boolean;
    campaignsList: string[];
    datasourceList: string[];
    campaignFilters: CampaignFilters;
    onApply: (campaignFilters: CampaignFilters) => void;
}