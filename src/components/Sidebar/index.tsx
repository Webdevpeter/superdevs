import React, { useState } from 'react';
import styled from 'styled-components';
import Select from 'react-select';

import { SidebarProps } from './types';
import mapToOptions from '../../helpers/mapToSelectOptions';
import { CampaignFilters } from '../../types';

const Wrapper = styled.aside`
    width: 30%;
    padding: 20px;
    background: gray;
`;

const FormGroup = styled.div`
    margin-bottom: 10px;
`;

const Label = styled.label`
    display: block;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 5px;
`;

const ApplyButton = styled.button`
    border-radius: 5px;
    padding: 10px;
    cursor: pointer;
`;

function Sidebar(props: SidebarProps) {
    const { campaignsList, datasourceList, isLoading, campaignFilters, onApply } = props;
    const [filters, setFilters] = useState<CampaignFilters>(campaignFilters);

    return (
        <Wrapper>
            <h2>Filter dimension values</h2>
            <FormGroup>
                <Label htmlFor="datasourcesSelect">Datasource</Label>
                <Select
                    id={'datasourcesSelect'}
                    isMulti={true}
                    name="datasources-select"
                    options={mapToOptions(datasourceList)}
                    onChange={(datasources) => {
                        setFilters({
                            ...filters,
                            datasources: datasources.map(datasource => datasource.value)
                        });
                    }}
                    isLoading={isLoading}
                />
            </FormGroup>
            <FormGroup>
                <Label htmlFor="campaignsSelect">Campaign</Label>
                <Select
                    id={'campaignsSelect'}
                    isMulti={true}
                    name="campaigns-select"
                    options={mapToOptions(campaignsList)}
                    onChange={(campaigns) => {
                        setFilters({
                            ...filters,
                            campaigns: campaigns.map(campaign => campaign.value)
                        });
                    }}
                    isLoading={isLoading}
                />
            </FormGroup>
            <ApplyButton onClick={() => {onApply(filters)}}>Apply</ApplyButton>
        </Wrapper>
    );
}

export default Sidebar;