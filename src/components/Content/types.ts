import { CampaignFilters, CampaignStats } from "../../types";

export interface ContentProps {
    campaignFilters: CampaignFilters;
    stats: CampaignStats[];
}