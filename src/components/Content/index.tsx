import React from 'react';
import styled from 'styled-components';

import printFilters from '../../helpers/printFilters';
import { ContentProps } from './types';
import Chart from '../Chart';

const Main = styled.main`
    display: flex;
    flex: 1;
    padding: 20px;
    flex-direction: column;
`;

const ChartWrapper = styled.div`
    flex: 1;
`;

function Content(props: ContentProps) {
    const { stats, campaignFilters } = props;

    return (
        <Main>
            <p>{`Datasources: ${printFilters(campaignFilters.datasources)}`}</p>
            <p>{`Campaigns: ${printFilters(campaignFilters.campaigns)}`}</p>
            <ChartWrapper>
                <Chart
                    stats={stats}
                    campaignFilters={campaignFilters}
                />
            </ChartWrapper>
        </Main>
    );
}

export default Content;