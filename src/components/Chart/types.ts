import { CampaignFilters, CampaignStats } from "../../types";

export interface ChartProps {
    campaignFilters: CampaignFilters;
    stats: CampaignStats[];
}