import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import { ChartProps } from './types';
import filterCampaignStats from '../../helpers/filterCampaignStats';
import mapToChartData from '../../helpers/mapToChartData';

function Chart(props: ChartProps) {
    const { stats, campaignFilters } = props;
    const chartData = mapToChartData(filterCampaignStats(campaignFilters, stats));

    return (
        <ResponsiveContainer width="100%">
            <LineChart
                data={chartData}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Date" />
                <YAxis yAxisId="left" label={{ value: 'Clicks', angle: -90, position: 'insideLeft' }} />
                <YAxis yAxisId="right" orientation="right" label={{ value: 'Impressions', angle: 90, position: 'insideRight' }} />
                <Tooltip />
                <Legend />
                <Line yAxisId="left" type="monotone" dataKey="Clicks" stroke="#8884d8" activeDot={{ r: 8 }} />
                <Line yAxisId="right" type="monotone" dataKey="Impressions" stroke="#82ca9d" />
            </LineChart>
        </ResponsiveContainer>
    );
}

export default Chart;